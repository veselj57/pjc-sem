# Řešení lineárních rovnic pomocí GEM
- Program umí řešit všechny typy lineárních rovnic ve tvaru Ax = b.
- Matice A může být ve tvaru M x N.
- Podpora paralelní zpracování v pomocí n vláken, n je parametr v programu.


### Parametry programu
- `sequential <rows> <columns> <txt input file>`
- `sequential-test <rows> <columns>`
- `parallel <threads> <rows> <columns> <txt input file>`
- `parallel-test <threads> <rows> <columns>`
- `--help`

### Příklady výpočtu
1 1 1  1<br />
1 1 1  1<br />
1 1 1  1<br />
Infinitely many solutions: span:  (-1 1 0 ) (-1 0 1 )  particular solution:  1 0 0 

1 1 1  1<br />
1 1 1  1<br />
0 0 0  1<br />
No solution

1 2 3  1<br />
1 5 9  1<br />
3 3 7  1<br />

One solution: 0.5, 1, -0.5

1 2 3<br />
1 5 9<br />
3 3 7<br />
No solution

### Výpočetní algorytmus
```
int col = 0, row = 0;
while (col < m.cols-1 && row < m.rows){
    int max = max_value_in_col(row, col);

    // All values in column are zero, no need to reduce
    if (max == -1){
        col++;
        continue;
    }

    // Do not swap same rows
    if (max != row){
        // Swap max with current
        for (int k = row; k < m.cols; k++) 
            swap_var(max, row, k);
    }

    // Reduce rows
    for (int k = row + 1; k < m.rows; k++) 
        reduce_row(row, k, col);
    
    pivots.push_back(col);
    row++;
    col++;
}
```


### Naměřené časy
Jednovlánový běh CCA 14 - 15 vteřin pro matici 3000 x 3001
Paralelní běh se 4 vlákny cc 11-13 vteřiny pro stejnou matici

Vícevláknové swapování řádků  robíhá  cols/threads proměných na jedno válkno. 
Šlo by zlepšit přehazováním pointrů na jednotlivé řádky, ale tento postup mě napadl až při konečném testování.

Redukování řádků probíhá podobně, jedno válkno redukuju rows/threads řádků.

Optimální na mém počítači jsou 4 vlákna, jelikož mám 4 jádrový procesor
U více vláken se sice výsledek o stovky milisekund zlepšoval i při zapojení  cca 7 vláken , ale zlešení nebylo tak výranzné.




