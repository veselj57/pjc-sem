/* Copyright (C) 2019 Jan Veselý */

#include <utility>

#include <iostream>
#include <vector>

#include <iostream>
#include <cmath>
#include <chrono>
#include <random>
#include <iomanip>
#include <thread>
#include <math.h>
#include <string.h>
#include <fstream>
#include <chrono>

namespace pjc{

    /**
     * Holds information about the matrix
     */
    class matrix{
    public: const int cols;
    public: const int rows;

    private: std::vector<double> data;

    public:
        matrix(unsigned int cols, unsigned int rows, std::vector<double> data):
                cols(cols),
                rows(rows),
                data(std::move(data))
        {};

        double& get_element(int eq, int var){
            return  data.at(eq*cols + var);
        }

        double& operator() (int eq, int var){
            return get_element(eq, var);
        }

        void print(){
            for(int e = 0; e <rows; e++){
                for (int v = 0; v < cols; v++) {
                    std::cout << std::setw(8) << (round(get_element(e, v) * 100.0 ) / 100.0)  << " ";

                    if (v == cols-2)
                        std::cout << "| ";
                }
                std::cout << std::endl;
            }
        }

    };

    /**
     * class for solving matrix equations.
     */
    class matrix_solver {
    private:
        matrix m;
        std::vector<int> pivots;

    public:
        struct solution{
            enum class kind {
                NONE, ONE, INFINIT
            };

            kind kind;
            std::vector<double> vars;
            std::vector<std::vector<double>> particular_solution;

            friend std::ostream& operator<<(std::ostream& os, const solution& s) {
                switch (s.kind){
                    case kind::NONE:
                        os << "No solution";
                        break;
                    case kind::ONE: {
                        os << "One solution => ";
                        bool first = true;
                        for (auto &d : s.vars) {
                            if (first)
                                first = false;
                            else
                                os << ", ";
                            std::cout << d;
                        }
                        break;
                    }case kind::INFINIT:{
                        os << "Infinitely many solutions| ";

                        os << "Span => ";
                        for (auto &p : s.particular_solution) {
                            os << "( ";
                            for (auto &e : p)
                                os << round( e * 100) / 100 << " ";
                            os << ") ";
                        }
                        os << ", Particular solution => ( ";
                        for (auto &e : s.vars)
                            os << round(e * 100) / 100 << " ";
                        os << ")";

                        break;
                    }
                }
                os << std::endl;

                return os;
            }
        };

        explicit matrix_solver(matrix m) : m(std::move(m)) {};

        /**
         * Reduce matrix to upper triangular form using sequential approach
         * @param threads
         */
        void sequential_compute() {
            int col = 0, row = 0;
            while (col < m.cols-1 && row < m.rows){
                int max = max_value_in_col(row, col);

                // If all values in the column are zero, there is no need to reduce
                if (max == -1){
                    col++;
                    continue;
                }

                // Do not swap same rows
                if (max != row){
                    // Swap max with current
                    for (int k = row; k < m.cols; k++)
                        swap_var(max, row, k);
                }

                // Reduce rows
                for (int k = row + 1; k < m.rows; k++)
                    reduce_row(row, k, col);

                pivots.push_back(col);
                row++;
                col++;
            }
        }

        /**
         * Reduce matrix to upper triangular form using parallel approach
         * @param pool_size
         */
        void parallel_compute(unsigned int pool_size) {

            //distribute work evenly between threads
            int cols_per_thread = (m.cols + pool_size - 1) / pool_size;
            int rows_per_thread = (m.rows + pool_size - 1) / pool_size;

            int col = 0, row = 0;
            while (col < m.cols-1 && row < m.rows){
                int max = max_value_in_col(row, col);

                // All values in column are zero, no need to reduce
                if (max == -1){
                    col++;
                    continue;
                }

                // Do not swap same rows
                if (max != row){
                    std::vector<std::thread> thread_pool;

                    int p_cols = 0;
                    while (p_cols < m.cols){
                        int batch = cols_per_thread;

                        if (p_cols + cols_per_thread > m.cols)
                            batch = m.cols - p_cols;

                        std::thread t(&matrix_solver::parallel_swap_var, this, row, max, p_cols, batch);
                        thread_pool.push_back(std::move(t));

                        p_cols += batch;
                    }

                    for (std::thread  &t : thread_pool)
                        t.join();
                }

                std::vector<std::thread> thread_pool;

                int p_row = row  + 1;
                while (p_row < m.rows){
                    int batch = rows_per_thread;

                    if (p_row + rows_per_thread >= m.rows)
                        batch = m.rows - p_row;

                    std::thread t(&matrix_solver::parallel_reduction, this, row, p_row, batch, col);
                    thread_pool.push_back(std::move(t));

                    p_row += rows_per_thread;
                }

                for (std::thread  &t : thread_pool)
                    t.join();

                pivots.push_back(col);
                row++;
                col++;

            }
        }

        bool is_zero(double &x){
            return abs(x) < 0.00000001;
        }

        /**
         * Collect result from reduced reduced matrix
         */
        pjc::matrix_solver::solution collect_result(){

            int rank = pivots.size();

            // All bottom zero rows must have zero b vector
            for (int j = m.rows -1; j >  rank -1 ; j--) {
                if (!is_zero(m(j, m.cols-1))){
                    return {pjc::matrix_solver::solution::kind::NONE};
                }
            }

            std::vector<double> solution(m.cols-1);

            //RANK == n there is only one solution;
            if (rank == m.cols-1){
                for (int i=m.cols-2; i>=0; i--){
                    solution[i] = m(i, m.cols-1)/m(i, i);
                    for (int k=i-1;k>=0; k--) {
                        m(k, m.cols-1) -= m(k, i) * solution[i];
                    }
                }
                return {pjc::matrix_solver::solution::kind::ONE, solution};
            }

            std::vector<std::vector<double>> span;
            span.resize(m.cols - 1 - rank);



            //vectors which send x to ker
            int ones = 0;
            for (auto &span_vec : span) {
                // Prepare span vectors, set pivots to NAN and
                // make them linearly independent
                span_vec.resize(m.cols-1);
                std::fill(span_vec.begin(), span_vec.end(), 0);

                for (auto &x : pivots)
                    span_vec[x] = NAN;

                while ( isnan(span_vec[ones]))
                    ones++;

                span_vec[ones] = 1;
                ones++;

                // Calculate pivot values
                int piv_iter = pivots.size() -1;
                for (int row =  rank-1; row >=0 ; row--) {
                    double tmp = 0;

                    for (int col = pivots[piv_iter]+1 ; col< m.cols-1; col++)
                        tmp -=  m(row, col) * span_vec[col];

                    span_vec[pivots[piv_iter]] =  tmp/m(row, pivots[piv_iter]);
                    piv_iter--;
                }
            }

            // calculate particular solution
            int piv_iter = pivots.size() -1;
            std::fill(solution.begin(), solution.end(), 0);
            for (int row =  rank-1; row >=0 ; row--) {
                double tmp = m(row, m.cols-1);

                for (int col = pivots[piv_iter]+1 ; col< m.cols-1; col++)
                    tmp -=  m(row, col) * solution[col];

                solution[pivots[piv_iter]] =  tmp/m(row, pivots[piv_iter]);
                piv_iter--;
            }
            return {pjc::matrix_solver::solution::kind::INFINIT, solution, span};
        }

    private:
        /**
         * Parallel column reduction. Batch reduce
         */
        void parallel_reduction(int pivot_row, int from, int rows, int col){
            //std::cout << "from: " << from << ", batch: " << rows << std::endl;
            for (int i = from; i < from + rows ; i++) {
                reduce_row(pivot_row, i, col);
            }
        }

        /**
        * Swap var from first to second row at position var
        */
        void parallel_swap_var(int first_r, int second_r, int from, int batch){
            for (int i = from; i < from + batch ; i++) {
                swap_var(first_r, second_r, i);
            }
        }

        /**
         * Find maximum element in diagonal i
         */
        int max_value_in_col(int row, int col){
            double max_in_col = std::abs(m(row, col));
            int max_row = row;

            for (int k = row + 1; k < m.rows; k++) {
                if (std::abs(m(k, col)) > max_in_col) {
                    max_in_col = std::abs(m(k, col));
                    max_row = k;
                }
            }

            if (abs(max_in_col) < 0.000000001){
                return -1;
            }

            return max_row;
        };

        /**
         * Swap war from first to second row at position var
         */
        void swap_var(int first_r, int second_r, int var){
            double tmp = m(first_r, var);
            m(first_r, var) = m(second_r, var);
            m(second_r, var) = tmp;
        }

        /**
         * Reduce given column
         */
        void reduce_row(int pivot_row, int to_be_reduced, int col){
            double c = -m(to_be_reduced, col) / m(pivot_row, col);

            if (c == 0){
                return;
            }

            for (int j = col; j < m.cols; j++) {
                m(to_be_reduced, j) += c * m(pivot_row, j);
            }
        }

    };

};

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

double get_random_double() {
    static std::mt19937 mt{ std::random_device{}() };
    static std::uniform_real_distribution<> dist(-1000, 1000);
    return dist(mt);
}

pjc::matrix_solver get_random(unsigned int rows, unsigned int cols){
    std::vector<double> data1 = std::vector<double>(cols*rows);

    for (unsigned long i = 0; i < cols*rows; i++) {
        data1.at(i) = get_random_double();
    }
    return pjc::matrix_solver(pjc::matrix(cols, rows, data1));
}

std::vector<double> read_input_data(const std::string &name, int count){
    std::fstream file(name, std::ios_base::in);
    std::vector<double> data(count);

    for (auto &item : data){
        file >> item;
    }
    return data;
}


unsigned int parse_number(const std::string &number){
    try {
        return static_cast<unsigned int>(std::stoi(number));
    }catch (...) {
        std::cout<< "Parse error:'"<< number << "' is not number.";
        exit(0);
    }
}

int main(int argc, char *argv[]) {
    auto &os = std::cout;

    if (argc == 2 && strcmp(argv[1], "--help") == 0){
        os << "sequential <rows> <columns> <txt input file>" <<std::endl;
        os << "sequential-test <rows> <columns>" <<std::endl;
        os << "parallel <threads> <rows> <columns> <txt input file>" <<std::endl;
        os << "parallel-test <threads> <rows> <columns>" <<std::endl;
    } else if (argc >= 2){

        if (strcmp(argv[1], "sequential") == 0){
            if(argc != 5){
                os << "Invalid number for command:sequential, 4 is required";
                exit(0);
            }

            unsigned int rows = parse_number(argv[2]);
            unsigned int cols = parse_number(argv[3]);


            std::vector<double> data = read_input_data(argv[4], cols*rows);
            auto x = pjc::matrix(cols, rows, std::move(data));
            //x.print();
            pjc::matrix_solver s_solver(x);

            auto start = std::chrono::high_resolution_clock::now();
            s_solver.sequential_compute();
            os<<s_solver.collect_result();
            auto end = std::chrono::high_resolution_clock::now();
            //std::cout << "Needed " << to_ms(end - start).count() << " ms to finish." <<std::endl;

        } else if (strcmp(argv[1], "sequential-test") == 0){
            if(argc != 4){
                os << "Invalid number for command:sequential-test, 3 is required";
                exit(0);
            }

            auto s_solver = get_random(parse_number(argv[2]), parse_number(argv[3]));
            auto start = std::chrono::high_resolution_clock::now();
            s_solver.sequential_compute();
           // os<<s_solver.collect_result();
            auto end = std::chrono::high_resolution_clock::now();
            std::cout << "Needed " << to_ms(end - start).count() << " ms to finish." <<std::endl;

        } else if (strcmp(argv[1], "parallel") == 0){
            if(argc != 6){
                os << "Invalid number for command:parallel, 5 is required";
                exit(0);
            }

            unsigned int pool= parse_number(argv[2]);
            unsigned int rows = parse_number(argv[3]);
            unsigned int cols = parse_number(argv[4]);

            if(pool > cols){
                os << "Cant have more threads than rows.";
                exit(0);
            }

            std::vector<double> data = read_input_data(argv[5], cols*rows);
            auto x = pjc::matrix(cols, rows, std::move(data));
            //x.print();
            pjc::matrix_solver s_solver(x);

            auto start = std::chrono::high_resolution_clock::now();
            s_solver.parallel_compute(pool);
            os<<s_solver.collect_result();
            auto end = std::chrono::high_resolution_clock::now();
            //std::cout << "Needed " << to_ms(end - start).count() << " ms to finish." <<std::endl;

        } else if (strcmp(argv[1], "parallel-test") == 0){
            if(argc != 5){
                os << "Invalid number for command:parallel-test, 4 is required";
                exit(0);
            }

            unsigned int pool= parse_number(argv[2]);
            unsigned int rows = parse_number(argv[3]);
            unsigned int cols = parse_number(argv[4]);

            if(pool > cols){
                os << "Cant have more thread than rows.";
                exit(0);
            }

            auto s_solver = get_random(rows, cols);
            auto start = std::chrono::high_resolution_clock::now();
            s_solver.parallel_compute(parse_number(argv[2]));
            //os<<s_solver.collect_result();
            auto end = std::chrono::high_resolution_clock::now();
            std::cout << "Needed " << to_ms(end - start).count() << " ms to finish." <<std::endl;

        } else{
            os<<"Unknown command." << std::endl;
        }
    } else{
        os<<"Please input an action." << std::endl;
    }

}